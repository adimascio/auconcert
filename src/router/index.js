import Vue from 'vue';
import Router from 'vue-router';
import Search from '@/components/Search';
import Concert from "@/components/Concert";
import ConcertList from "@/components/ConcertList";
import Work from "@/components/Work";

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Search',
            component: Search,
        },
        {
            path: '/concerts',
            name: 'ConcertList',
            component: ConcertList,
        },
        {
            path: '/performance/:performance',
            name: 'Concert',
            component: Concert,
        },
        {
            path: '/work/:work',
            name: 'Work',
            component: Work,
        },
    ],
});
