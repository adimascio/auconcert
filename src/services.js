import {
    bnfFetchWork,
    sparqlexec,
} from './lib/sparql';


export function workinfos(workid) {
    return bnfFetchWork(workid);
}

export async function workFromEan(ean) {
    const results = await sparqlexec('http://data.bnf.fr/sparql', `
    PREFIX bnf-onto: <http://data.bnf.fr/ontology/bnf-onto/>
    PREFIX rdarelationships: <http://rdvocab.info/RDARelationshipsWEMI/>

    SELECT ?bnfid WHERE {
        ?x bnf-onto:ean "${ean}" ;
            rdarelationships:workManifested ?work.
        ?concept foaf:focus ?work ;
            bnf-onto:FRBNF ?bnfid.
    }`);
    return results.length ? results[0].bnfid : null;
}
