// require('es6-promise').polyfill();
require('isomorphic-fetch');

const CACHE = new Map();
const ROLES = {
    r70: 'auteur du texte',
    r80: "auteur de l'argument",
    r160: 'chorégraphe',
    r220: 'compositeur',
};

const DATABNF_PREFIXES = {
    rdf: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    rdfs: 'http://www.w3.org/2000/01/rdf-schema#',
    skos: 'http://www.w3.org/2004/02/skos/core#',
    foaf: 'http://xmlns.com/foaf/0.1/',
    xfoaf: 'http://www.foafrealm.org/xfoaf/0.1/',
    dcmitype: 'http://purl.org/dc/dcmitype/',
    ore: 'http://www.openarchives.org/ore/terms/',
    ark: 'http://ark.bnf.fr/ark:/12148/',
    dbpedia: 'http://dbpedia.org/',
    dbpediaowl: 'http://dbpedia.org/ontology/',
    dbprop: 'http://dbpedia.org/property/',
    rdagroup2elements: 'http://rdvocab.info/ElementsGr2/',
    frbr: 'http://rdvocab.info/uri/schema/FRBRentitiesRDA/',
    rdarole: 'http://rdvocab.info/roles/',
    rdagroup1elements: 'http://rdvocab.info/Elements/',
    rdarelationships: 'http://rdvocab.info/RDARelationshipsWEMI/',
    og: 'http://ogp.me/ns#',
    'bnf-onto': 'http://data.bnf.fr/ontology/bnf-onto/',
    dcterms: 'http://purl.org/dc/terms/',
    owl: 'http://www.w3.org/2002/07/owl#',
    time: 'http://www.w3.org/TR/owl-time/',
    marcrel: 'http://id.loc.gov/vocabulary/relators/',
    bnfroles: 'http://data.bnf.fr/vocabulary/roles/',
    mo: 'http://purl.org/ontology/mo/',
    geo: 'http://www.w3.org/2003/01/geo/wgs84_pos#',
    ign: 'http://data.ign.fr/ontology/topo.owl/',
    insee: 'http://rdf.insee.fr/geo/',
    gn: 'http://www.geonames.org/ontology/ontology_v3.1.rdf/',
    dcdoc: 'http://dublincore.org/documents/',
    bio: 'http://vocab.org/bio/0.1/',
    isni: 'http://isni.org/ontology#',
    bibo: 'http://purl.org/ontology/bibo/',
    schema: 'http://schema.org/',
};

/**
 * > ns_prop('http://www.w3.org/2004/02/skos/core#prefLabel',
              DatabnfDatabase.namespaces)
   'skos:prefLabel'
 *
 * @param {*} uri
 * @param {*} namespaces
 */
export function nsprop(uri, namespaces) {
    for (const ns of Object.keys(namespaces)) {
        const prefix = namespaces[ns];
        if (uri.startsWith(prefix)) {
            return `${ns}:${uri.slice(prefix.length)}`;
        }
    }
    return uri;
}

export function simplifyData(data, namespaces) {
    const props = {};
    for (let { attr, value } of data) {
        if (attr === undefined) {
            continue;
        }
        attr = nsprop(attr, namespaces);
        if (props[attr] !== undefined) {
            const propval = props[attr];
            if (propval instanceof Array) {
                propval.push(value);
            } else {
                props[attr] = [propval, value];
            }
        } else {
            props[attr] = value;
        }
    }
    return props;
}

function exposeData(data) {
    const normalizeMap = {
        'skos:note': 'notes',
        'skos:prefLabel': 'prefLabel',
        'skos:altLabel': 'altLabels',
        'bnf-onto:lastYear': 'year',
        'rdarelationships:expressionOfWok': 'expressions',
        'dcterms:description': 'description',
        'mo:genre': 'genre',
        contributors: 'contributors',
        'foaf:depiction': 'depictions',
        'dbpedia:abstract': 'abstract',
        'http://data.bnf.fr/vocabulary/roles/r70': 'r70',
        'http://data.bnf.fr/vocabulary/roles/r80': 'r80',
        'http://data.bnf.fr/vocabulary/roles/r160': 'r160',
        'http://data.bnf.fr/vocabulary/roles/r220': 'r220',
        eans: 'eans',
    };
    const exposed = {};
    for (const key of Object.keys(normalizeMap)) {
        if (data[key] !== undefined) {
            exposed[normalizeMap[key]] = data[key];
        }
    }
    return exposed;
}

function parseJsonResults(rset) {
    const results = [];
    for (const row of rset.results.bindings) {
        const result = {};
        for (const varname of rset.head.vars) {
            if (row[varname] === undefined) {
                result[varname] = null;
            } else {
                result[varname] = row[varname].value;
            }
        }
        results.push(result);
    }
    return results;
}

export async function sparqlexec(endpoint, query) {
    const format = 'application/sparql-results+json';
    const result = await fetch(
        `${endpoint}?query=${encodeURIComponent(query)}&format=${encodeURIComponent(
            format
        )}&timeout=0`
    );
    return parseJsonResults(await result.json());
}

export async function fetchFromDbpedia(uri) {
    const dbpediaInfos = await sparqlexec(
        'http://fr.dbpedia.org/sparql',
        `
    SELECT ?abstract WHERE {
        <${uri}> <http://dbpedia.org/ontology/abstract> ?abstract.
        FILTER (lang(?abstract) = 'fr')
    }`
    );
    return dbpediaInfos.length === 1 ? dbpediaInfos[0] : null;
}

function ark2notice(noticeid) {
    const match = /ark:\/12148\/cb([0-9]{8})/.exec(noticeid);
    if (match !== null) {
        noticeid = match[1];
    }
    return noticeid;
}

export async function bnfFetchAuthority(noticeid) {
    const match = /ark:\/12148\/cb([0-9]{8})/.exec(noticeid);
    if (match !== null) {
        noticeid = match[1];
    }
    if (CACHE.has(noticeid)) {
        return CACHE.get(noticeid);
    }
    let rawData = await sparqlexec(
        'http://data.bnf.fr/sparql',
        `
    SELECT ?attr ?value WHERE {
        ?work bnf-onto:FRBNF "${noticeid}"^^xsd:integer.
        ?work ?attr ?value.
    }`
    );
    rawData = rawData.concat(
        await sparqlexec(
            'http://data.bnf.fr/sparql',
            `
    SELECT ?attr ?value WHERE {
        ?concept bnf-onto:FRBNF "${noticeid}"^^xsd:integer.
        ?concept foaf:focus ?work.
        ?work ?attr ?value.
    }`
        )
    );
    rawData = simplifyData(rawData, DATABNF_PREFIXES);
    if (rawData['owl:sameAs'] !== undefined && rawData['owl:sameAs'] instanceof Array) {
        for (const uri of rawData['owl:sameAs']) {
            if (uri.startsWith('http://fr.dbpedia.org')) {
                rawData['dbpedia:abstract'] = await fetchFromDbpedia(uri);
            }
        }
    }
    rawData = exposeData(rawData);
    CACHE.set(noticeid, rawData);
    return rawData;
}

export async function fetchEANs(workid) {
    const eanData = await sparqlexec(
        'http://data.bnf.fr/sparql',
        `
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdarelationships: <http://rdvocab.info/RDARelationshipsWEMI/>
    PREFIX bnfroles: <http://data.bnf.fr/vocabulary/roles/>

    select distinct ?ean where {
        ?concept bnf-onto:FRBNF "${workid}"^^xsd:integer ;
               foaf:focus ?work.
        ?manifestation bnf-onto:ean ?ean ;
                       rdarelationships:workManifested ?work.
        }`
    );
    return eanData.map(row => row.ean);
}

export async function bnfFetchWork(workid) {
    let workData = await bnfFetchAuthority(workid);
    workData.eans = await fetchEANs(workid);
    let contributorRoles = await sparqlexec(
        'http://data.bnf.fr/sparql',
        `
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdarelationships: <http://rdvocab.info/RDARelationshipsWEMI/>
    PREFIX bnfroles: <http://data.bnf.fr/vocabulary/roles/>

    select distinct ?authorConcept ?rel where {
      ?concept bnf-onto:FRBNF "${workid}"^^xsd:integer ;
               foaf:focus ?work.
      ?work rdarelationships:expressionOfWork ?expr.
      ?expr owl:sameAs ?newExpr.
      ?newExpr ?rel ?author.
      ?oldAuthor owl:sameAs ?author.
      ?authorConcept foaf:focus ?oldAuthor.
      {
          ?work dcterms:contributor ?oldAuthor
      }
      UNION
      {
          ?work dcterms:creator ?oldAuthor
      }
      FILTER REGEX(?rel, '^http://data.bnf.fr/vocabulary/roles/(${Object.keys(ROLES).join('|')})')
    }    `
    );
    const done = new Set();
    const contributors = {};
    for (let { authorConcept, rel } of contributorRoles) {
        rel = ROLES[rel.slice(rel.lastIndexOf('/') + 1)];
        if (done.has(authorConcept)) {
            continue;
        }
        done.add(authorConcept);
        const authordata = await bnfFetchAuthority(authorConcept);
        if (contributors[rel] === undefined) {
            contributors[rel] = [authordata];
        } else {
            contributors[rel].push(authordata);
        }
    }
    workData.contributors = contributors;
    return workData;
}

export async function fetchPerformanceInfos(perfId) {
    const perfUri = `<http://data.doremus.org/performance/${perfId}>`;
    const perfInfos = await sparqlexec('http://data.doremus.org/sparql', `
    SELECT ?date ?title ?url ?program WHERE {
        ${perfUri} <http://data.doremus.org/ontology#U77_foresees_performing_plan> ?program;
            rdfs:label ?title ;
            <http://data.doremus.org/ontology#U8_foresees_time_span> ?interval.
        ?interval rdfs:label ?date.
        OPTIONAL {
            ${perfUri}  <http://xmlns.com/foaf/0.1/isPrimaryTopicOf> ?url.
        }
    }
    `);
    return {
        date: perfInfos[0].date,
        title: perfInfos[0].title,
        url: perfInfos[0].url,
        works: await fetchWorks(perfInfos[0].program),
    }
}

export async function fetchWorks(perfId) {
    perfId = perfId.slice(perfId.lastIndexOf('/') + 1);
    const rset = await sparqlexec(
        'http://data.doremus.org/sparql',
        `
    SELECT ?expression ?title ?comment ?oeuvrebnf ?databnf WHERE {
        <http://data.doremus.org/expression/${
            perfId
        }> <http://erlangen-crm.org/current/P165_incorporates> ?expression .
        ?expression rdfs:label ?title .
        OPTIONAL {
            ?expression rdfs:comment ?comment .
        } .
        OPTIONAL {
            ?expression owl:sameAs ?oeuvrebnf .
            ?oeuvrebnf owl:sameAs ?databnf.
        }
    } GROUP BY ?expression`
    );
    return rset.map(row => ({
        bnfid: ark2notice(row.databnf),
        uri: row.expression,
        doremusbnf: row.oeuvrebnf,
        comment: row.comment,
        title: row.title,
    }));
}


export async function ftisearchWorks(text) {
    const rset = await sparqlexec('http://data.bnf.fr/sparql', `
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX bif: <bif:>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX dcterms: <http://purl.org/dc/terms/>
    PREFIX rdarelationships: <http://rdvocab.info/RDARelationshipsWEMI/>

    SELECT ?concept ?label ?pconcept ?plabel (COUNT(?expr) AS ?count) WHERE {
      ?concept foaf:focus ?work ;
        skos:prefLabel ?label.
        ?label bif:contains "'${text}*'".
        ?work a <http://rdvocab.info/uri/schema/FRBRentitiesRDA/Work> ;
        dcterms:creator ?person.
      ?pconcept foaf:focus ?person;
        skos:prefLabel ?plabel.
      ?work rdarelationships:expressionOfWork ?expr.
    }
    GROUP BY ?concept ?label ?pconcept ?plabel
    ORDER BY DESC(?count)
    LIMIT 10
    `);
    return rset.map(row => ({
        bnfid: ark2notice(row.concept),
        uri: null,
        doremusbnf: null,
        comment: null,
        title: row.label,
        compuri: row.pconcept,
        complabel: row.plabel,
    }));
}

export async function fetchConcertsWithDatabnfWorks() {
    return await sparqlexec('http://data.doremus.org/sparql', `
    SELECT DISTINCT ?program ?concert ?concertLabel ?concertUrl ?date WHERE {
        ?program a <http://erlangen-crm.org/efrbroo/F25_Performance_Plan> ;
            <http://erlangen-crm.org/current/P165_incorporates> ?expression .
            ?expression owl:sameAs ?oeuvrebnf .?oeuvrebnf owl:sameAs ?databnf.
        ?concert <http://data.doremus.org/ontology#U77_foresees_performing_plan> ?program ;
            rdfs:label ?concertLabel ;
            <http://data.doremus.org/ontology#U8_foresees_time_span> ?interval.
        ?interval rdfs:label ?date.
        OPTIONAL {
            ?concert <http://xmlns.com/foaf/0.1/isPrimaryTopicOf> ?concertUrl.
        }
    } ORDER BY DESC(?date) LIMIT 50
    `);
}
