'use strict'

const { app, webpackMiddleware } = require('../server');
const port = process.env.CODECEPT_PORT || 3000;
const uri = `http://localhost:${port}`;

let _resolve
const readyPromise = new Promise(resolve => {
    _resolve = resolve;
})

webpackMiddleware.waitUntilValid(() => {
    _resolve()
})

const server = app.listen(port)

module.exports = {
    ready: readyPromise,
    close: () => {
        server.close();
    },
};
