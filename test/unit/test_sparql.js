const { expect } = require('chai');
const sparql = require('../../src/lib/sparql');


describe('#nsprop()', () => {
    it('should identiy skos prefix', () => {
        expect(
            sparql.nsprop('http://www.w3.org/2004/02/skos/core#prefLabel', {
                skos: 'http://www.w3.org/2004/02/skos/core#',
            })
        ).to.equal('skos:prefLabel');
    });

    it('should leave unknown prefix', () => {
        expect(sparql.nsprop('http://www.w3.org/2004/02/skos/core#prefLabel', {})).to.equal(
            'http://www.w3.org/2004/02/skos/core#prefLabel'
        );
    });
});

describe('#simplifyData', () => {
    it('should gather attr/values and use ns prefixes when available', () => {
        expect(
            sparql.simplifyData(
                [
                    { attr: 'http://www.w3.org/2004/02/skos/core#prefLabel', value: 'Jean' },
                    { attr: 'http://www.w3.org/altlabel', value: 'VJ' },
                ], {
                    skos: 'http://www.w3.org/2004/02/skos/core#',
                },
            )
        ).to.deep.equal({
            'skos:prefLabel': 'Jean',
            'http://www.w3.org/altlabel': 'VJ',
        });
    });

    it('should create list of values on multiple occurrences', () => {
        expect(
            sparql.simplifyData(
                [
                    { attr: 'http://www.w3.org/2004/02/skos/core#prefLabel', value: 'Jean' },
                    { attr: 'http://www.w3.org/2004/02/skos/core#altLabel', value: 'VJ' },
                    { attr: 'http://www.w3.org/2004/02/skos/core#altLabel', value: 'VJean' },
                ], {
                    skos: 'http://www.w3.org/2004/02/skos/core#',
                },
            )
        ).to.deep.equal({
            'skos:prefLabel': 'Jean',
            'skos:altLabel': ['VJ', 'VJean'],
        });
    });
});
