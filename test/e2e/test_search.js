Feature('search');

Scenario('test search on databnf', I => {
    I.amOnPage('/');
    I.fillField('input[name=q]', 'Jeux');
    I.pressKey('Enter');
    I.waitForElement('#results ul li');
    I.see('Jeux d\'eau');
});
