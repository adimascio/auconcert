# tousauconcert

> Tous Au Concert

Le projet tous au concert est né lors du Hackathon organisé par la BnF en 2017.
Il s'agit d'une expérimentation permettant de récupérer des informations à propos
d'une œuvre musicale dans différents entrepôts de données (http://data.bnf.fr, http://data.doremus.org, http://gallica.bnf.fr, http://deezer.com).

## Requirements

- node >= 8.x

## Build Setup

``` bash
# install dependencies
npm install

node server.js

# alternatively

nodemon -w server.js -w src/lib/deezer.js -w src/lib/sru.js server.js

# build for production with minification
npm run build

```

## Build for production and run
```bash
npm run build

NODE_ENV=production node server.js
```

## Run tests
```bash
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```
