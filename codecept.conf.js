if (process.env.CODECEPT_URL === undefined) {
    process.env.CODECEPT_URL = 'http://localhost:3000';
}

exports.config = {
    tests: 'test/e2e/test_*.js',
    timeout: 10000,
    output: 'test/e2e/output',
    helpers: {
        Nightmare: {
            url: process.env.CODECEPT_URL,
        },
    },
    include: {
        I: './test/e2e/steps_file.js',
    },
    bootstrap: false,
    mocha: {},
    name: 'TousAuConcert',
};