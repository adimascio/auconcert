const path = require('path');

const express = require('express');

const {
    fetchSheetsFromGallica,
    fetchSoundFromGallica,
    fetchCriticsFromGallica,
} = require('./src/lib/sru');
const { fetchFromDeezer } = require('./src/lib/deezer');


const app = express();

let webpackMiddleware;

if (process.env.NODE_ENV === 'production') {
    app.use(express.static(path.join(__dirname, 'dist')));
} else {
    const webpack = require('webpack');
    const webpackConfig = require('./build/webpack.dev.conf');
    webpackConfig.output.publicPath = '/';
    webpackMiddleware = require('webpack-dev-middleware')(webpack(webpackConfig), {
        publicPath: "/",
    });
    app.use(webpackMiddleware);
}


function catcherror(fn) {
    return (req, res, next) => {
        Promise.resolve(fn(req, res, next)).catch(next);
    }
}

app.get(
    '/critics/:title/:author/:year',
    catcherror(async (req, res) => {
        res.json(
            await fetchCriticsFromGallica(req.params.title, req.params.author, req.params.year)
        );
    })
);


app.get(
    '/sheets/:title/:author',
    catcherror(async (req, res) => {
        res.json(await fetchSheetsFromGallica(req.params.title, req.params.author));
    })
);


app.get(
    '/sound/:title/:author',
    catcherror(async (req, res) => {
        res.json(await fetchSoundFromGallica(req.params.title, req.params.author));
    })
);

app.get(
    '/deezer/:eans',
    catcherror(async (req, res) => {
        res.json(await fetchFromDeezer(req.params.eans.split(',')));
    })
);

module.exports = {
    app,
    webpackMiddleware,
};

if (require.main === module) {
    // eslint-disable-next-line no-console
    app.listen(3000, () => console.log('TousAuConcert listening on port 3000!'));
}
